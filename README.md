# Saves your mobile chromium based browser tabs


## Requirements
adb (and already authorized devices)

curl

jq

[jd](https://github.com/josephburnett/jd) or docker (only needed when diffs are enabled)


## Installation
Copy the script to your path, for example `~/.local/bin/`

Setup the variables in the script:
```bash
# Specify aliases for browsers
declare -A browsers=( ["com.android.chrome"]="chrome" ["com.brave.browser"]="brave" ["com.kiwibrowser.browser"]="kiwi" )

# Set aliases for adb serial number
declare -A devices=( ["MYPHONE"]="xxxxxxxx" ["MYTABLET1"]="YYYYYYYYYYYYYYY" ["MYTABLET2"]="ZZZZZZZZZZZZZZZ" )

# Configure which browsers to save, empty means save all detected browsers
declare -A device_configs=( ["MYPHONE"]="chrome" ["MYTABLET1"]="brave" ["MYTABLET2"]="" )

# Create json diffs also
CREATE_DIFFS=0
```


## Usage
```bash
# Save tabs from all connected devices
savetabs

# Save tabs from a specific device
savetabs MYPHONE
```

All tabs are saved in a json file named:
```bash
tabs-${device}-${browser}_$(date +%Y-%m-%d-%H%M)_full.json
```

Diffs are saved in a json file named:
```bash
tabs-${device}-${browser}_$(date +%Y-%m-%d-%H%M)_diff.json
```

All files are saved in the current working directory


## Troubleshooting
Check `chrome://inspect/#devices` on your non-mobile browser


